<?php
namespace Nng\Nnfelogin\Controller;

use \TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Nng\Nnfelogin\Domain\Service\EncryptionService;
use Nng\Nnfelogin\Service\AuthentificationService;

/**
 * MainController
 */
class MainController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * @var \Nng\Nnfelogin\Domain\Repository\FrontendUserRepository
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 * @inject
	 */
	protected $frontendUserRepository;

	/**
	* @var \Nng\Nnfelogin\Domain\Service\FrontendUserService
	* @TYPO3\CMS\Extbase\Annotation\Inject
	* @inject
	*/
	protected $frontendUserService;
	
	/**
	* @var \Nng\Nnfelogin\Service\AuthentificationService
	* @TYPO3\CMS\Extbase\Annotation\Inject
	* @inject
	*/
	protected $authentificationService;
	
	/**
	* @var \Nng\Nnfelogin\Domain\Service\EncryptionService
	* @TYPO3\CMS\Extbase\Annotation\Inject
	* @inject
	*/
	protected $encryptionService;
	
	
	/**
	* SignalSlot Dispatcher
	*
	* @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher
	* @TYPO3\CMS\Extbase\Annotation\Inject
	* @inject
	*/
	protected $signalSlotDispatcher;

	protected $viewVars;
	
	protected $pathPartials;
	
	protected $isAjaxRequest;
	
	/**
	 * Initializes the current action
	 * @return void
	 */
	protected function initializeView( \TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view ) {
		$this->view->assignMultiple( $this->viewVars );	
	}

	/**
	 * Initializes the current action
	 * @return void
	 */
	protected function initializeAction() {

		$this->cObj = $this->configurationManager->getContentObject();
		
		$this->TS = \nn\t3::Settings()->getPlugin();
		$this->settings = \nn\t3::Settings()->getMergedSettings();

		// So können mehrere PlugIns auf einer Seite platziert werden, ohne einander gegenseitig zu beeinflussen		
		$this->_GPall = \nn\t3::Request()->GP();		
		$this->_GPvars = \nn\t3::Request()->GP('nnfelogin') ?: [];
		$this->uniqueId = $this->cObj->data['uid'];
	
		$this->_GP = (array) $this->_GPvars[$this->uniqueId];

		// Wurden Daten verschlüsselt übergeben? Dann dekodieren und Schlüssel erneuern
		$this->_GP = $this->encryptionService->decryptRequestArguments($this->_GP);
		$this->encryptionService->setJsEncryptionCookie();

		$this->viewVars = [
			'settings'			=> $this->settings,
			'_GP'				=> $this->_GP,
			'redirect_url'		=> $this->_GP['redirect_url'],
			'cObjData'			=> $this->cObj->data,
			'uniqueId'			=> $this->uniqueId,
			'_GPprefix'			=> 'nnfelogin['.$this->uniqueId.']',
			'feUser'			=> \nn\t3::FrontendUser()->getCurrentUser(),
			'baseUrl'			=> \nn\t3::Environment()->getBaseUrl(),
		];

		// Besondere Variablen mit übernehmen
		ArrayUtility::mergeRecursiveWithOverrule(
			$this->_GP, [
				'redirect_url'	=> GeneralUtility::sanitizeLocalUrl($this->_GPall['redirect_url'] ?: $this->_GP['redirect_url'])
			]
		);
		
	}


	/**
	 * 	Den eigentlichen View rendern.
	 * 	Berücksichtigt unterschiedliche Vorlagen aus dem Setup.
	 * 	@return string
	 */
	public function renderView() {
		$paths = $this->getTemplatePaths();
		\nn\t3::Template()->setTemplatePaths( $this->view, $paths );
		\nn\t3::Template()->removeControllerPath( $this->view );

		// keine AJAX-Antwort gewünscht? Dann View normal rendern
		if (!$this->_GP['ajax']) {
			return $this->view->render();
		}

		// AJAX-Response: Hier bewusst KEINE pageType oder eID genutzt, weil sonst 
		// die Einstellungen im Plugin nicht vorhanden sind.
		$viewVariables = \nn\t3::Template()->getVariables( $this->view );

		echo json_encode([
			'errors'	=> $viewVariables['errors'] ?: [],
			'redirect'	=> $viewVariables['redirect'] ?: '',
		]);

		die();		
	}

	private function getTemplatePaths() {
		$predefTemplate = $this->settings['predefTemplate'] ?: 'default';
		$templateSettings = $this->TS['predef'][$predefTemplate];
		return \nn\t3::Template()->mergeTemplatePaths( $this->TS['view'], $templateSettings );
	}
	

	/**
	 * action showFormAction
	 * Formular zeigen zum An / Abmelden eines fe_users
	 *
	 * @return void
	 */
	public function showLoginFormAction() {

		$_GP = $this->_GP;
		$view = [];

		// Benutzer bereits eingeloggt. Automatischer Redirect gewünscht?
		if ($this->settings['forceLoginRedirect'] && \nn\t3::FrontendUser()->isLoggedIn()) {
			if ($pid = $this->_GP['redirect_url']) \nn\t3::Http()->redirect($pid);
			if ($pid = $this->settings['loginRedirectPid']) \nn\t3::Http()->redirect( $pid );
		}

		// Benutzer hat E-Mail und Passwort eingegeben
		if ($_GP['validate']) {

			// Die eigentliche Authentifizierung passiert über \Nng\Nnfelogin\Service\AuthentificationService.
			// Wenn Benutzer korrekter Credentials eingegeben hat, ist er jetzt bereits als FE-User eingeloggt.

			if (\nn\t3::FrontendUser()->isLoggedIn()) {
				$user = \nn\t3::FrontendUser()->getCurrentUser();
				$userGroups = \nn\t3::FrontendUser()->getCurrentUserGroups();

				if (!count($userGroups)) {
					$view['errors'] = ['noGroup'=>1];
				} else {
					$pid = $this->settings['loginRedirectPid'] ?: \nn\t3::Page()->getPid();

					if (!$this->_GP['ajax']) {
						\nn\t3::Http()->redirect( $pid );
					} else {
						$view['redirect'] = \nn\t3::Http()->buildUri($pid, [], true);
					}
				}

			} else {
				$view['errors'] = ['pw' => 1, 'errorCode' => -5];
				$view['user'] = ['pw' => '', 'email' => substr(strip_tags($_GP['email']), 0, 60)];
			}
		}

		// Benutzer will sich ausloggen
		if ($_GP['logout'] || $_GET['logout']) {
			\nn\t3::FrontendUser()->logout();
			$pid = $this->settings['logoutRedirectPid'] ?: \nn\t3::Page()->getPid();
			if (!$this->_GP['ajax']) {
				\nn\t3::Http()->redirect( $pid );
			} else {
				$view['redirect'] = \nn\t3::Http()->buildUri($pid, [], true);
			}
		}
				
		$this->view->assignMultiple( $view );
		return $this->renderView();
	}
	
	

	/**
	 *  action resetPasswordDispatcher
	 *  "Passwort vergessen"-Funktion als Signal/Slot bereitstellen
	 *
	 *  $this->signalSlotDispatcher->dispatch('TYPO3\EsweApi\Domain\Service\UserService', 'emitResetPassword', array('email'=>'david@99grad.de'));
	 *
	 *  Signal
	 *  @param array $email
	 *  @return void
	 */
	public function resetPasswordDispatcher( $email ){

        $logger = GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
        $logger->info('NNFeLogin MainController:resetPasswordDispatcher', array('email' => json_encode($email) ));

		if (!trim($email)) return;
		if (!($user = $this->frontendUserRepository->findOneByEmail( $email ))) return;

		$this->initializeAction();
		$this->initializeView();
		
		$linkParams = $this->frontendUserService->generateForgotPasswordParams( $user->getUid() );

		$html = \nn\t3::Template()->render(
			'EmailResetPassword',
			array_merge($this->viewVars, ['feUser'=>$user], $linkParams),
			$this->getTemplatePaths()
		);

		\nn\t3::Mail()->send(array_merge($this->settings['forgotPassword'], [
			'toEmail'	=> $user->getEmail(),
			'html'		=> $html,
		]));
		
	}

	
	
	/**
	 *  action forgotPasswordFormAction
	 *  Formular für "Passwort vergessen"-Funktion
	 *
	 */
	public function showForgotPasswordFormAction () {
	
		$_GP = array_merge( (array) $this->_GP, (array) $_GET );
		
		$email = $_GP['email'];
		$view = array('data' => $_GP, 'errors'=>array());
		$view['mode'] = 'form';
		
		// Benutzer hat E-Mail (oder Kunden-Nummer / User-Name) eingegeben um Passwort zu ändern
		if ($_GP['validate']) {


			// MW: Workaround für Doppelte E-Mail Adressen
			$userTmp = $this->frontendUserRepository->findByCustomField( $_GP['email'], $this->settings['usernameFields'] );
			# $user = $this->frontendUserRepository->findByCustomField( $_GP['email'], $this->settings['usernameFields'] );
			
			$isEmail = !is_numeric(trim($_GP['email']));
			
			// MW: Workaround für Doppelte E-Mail Adressen
			$user = [];
			$user[0] = $userTmp[0];

			if (!is_array($user)) {

				// Keinerlei Benutzer gefunden
				$view['errors']['email'] = 1;
				$view['errors']['emailEntered'] = $isEmail;

			} else if (count($user) > 1) {

				// Benutzer konnte nicht eindeutig identifiziert werden, z.B. weil die E-Mail doppelt verwendet wurde
				$view['errors']['not_unique'] = 1;
				$view['errors']['emailEntered'] = $isEmail;
				
			} else {
			
				$user = array_shift($user);

				if (!trim($user['email'])) {
					$view['errors']['email'] = 2;
					$view['errors']['emailEntered'] = $isEmail;
					
				} else {

					// Alles klar – jetzt Mail versenden
					$view['mode'] = 'mailsent';

					$linkParams = $this->frontendUserService->generateForgotPasswordParams( $user['uid'] );

					// Falls statt der E-Mail eine Mitglieds-Nummer übergeben wurde: 
					// Kodiert zurückgeben zur Ausgabe im Frontend
					if ($this->viewVars['_GP']['email'] != $user['email']) {
						$this->viewVars['_GP']['email'] = EncryptionService::maskEmail( $user['email'] );
						$this->view->assignMultiple( $this->viewVars );
					}

					$html = \nn\t3::Template()->render(
						'EmailResetPassword',
						array_merge($this->viewVars, ['feUser'=>$user], $linkParams),
						$this->getTemplatePaths()
					);

					\nn\t3::Mail()->send(array_merge($this->settings['forgotPassword'], [
						'toEmail'	=> $user['email'],
						'html'		=> $html,
					]));

				}				
			}
		
		}
		
		
		// Benutzer hat auf den Link aus der E-Mail geklickt
		if ($_GP['forgothash']) {
			$user = $this->frontendUserRepository->findOneByUid( intval($_GP['user']) );

			if ($user && $this->frontendUserService->validateForgotPasswordHash( $_GP['user'], $_GP['forgothash'] ) ) {
				$view['mode'] = 'pwform';
				$view['forgothash'] = strip_tags($_GP['forgothash']);
				$view['user'] = strip_tags($_GP['user']);
				$view['feUser'] = $user;
			}
		}
		
		// Benutzer hat Formular für neues Passwort abgesendet
		if ($_GP['changepw'] && $_GP['forgothash']) {
			$user = $this->frontendUserRepository->findOneByUid( $_GP['user'], true );
			if ($user && $this->frontendUserService->validateForgotPasswordHash( $_GP['user'], $_GP['forgothash'] )) {
				if (strlen($_GP['pw']) < 6) {
					$view['errors']['pw_too_short'] = 1;
				} else if ($_GP['pw'] == $_GP['pw_confirm']) {
					$view['mode'] = 'pwchanged';
					$this->frontendUserService->updateUserPassword( $user->getUid(), $_GP['pw'] );
					$view['feUser'] = $user;	
				} else {
					$view['mode'] = 'pwform';
					$view['errors']['pw'] = 1;
				}
			}
		}

		$this->view->assignMultiple( $view );
		return $this->renderView();
	}
	
	
	/**
	 *  action showChangePasswordFormAction - Formular für "Passwort ändern"-Funktion
	 *	Kann für eingeloggte FE-User verwendet werden, die im Mitgliedsbereich ihr Passwort ändern möchten
	 */
	public function showChangePasswordFormAction () {
	
		if (!($user = \nn\t3::FrontendUser()->getCurrentUser())) {
			if ($pid = $this->settings['loginFormPid']) {
				\nn\t3::Http()->redirect( $pid, array('redirect_url'=>$GLOBALS['TSFE']->id) );
			}
			return 'Bitte loggen Sie sich ein, um diese Funktion zu nutzen.';
		}
		
		$_GP = $this->_GP;
		
		$old_pw = $_GP['pw'];
		$new_pw = $_GP['new_pw'];
		$new_pw_repeat = $_GP['new_pw_repeat'];
		
		$view = array('data' => $_GP, 'errors'=>array());
		$view['mode'] = 'change_pwform';

		if ($_GP['update_pw']) {

			if (!$old_pw) $view['errors']['pw'] = 1;
			if ($new_pw != $new_pw_repeat) $view['errors']['new_pw_not_identical'] = 1;
			if (!$new_pw)  $view['errors']['new_pw'] = 1;

			// Alles ok
			if (!$view['errors']) {
				if (strlen($new_pw) < 5) {
					$view['errors']['pw_too_short'] = 1;
				} else {					
					if ($user = $this->authentificationService->validate($user['uid'], $old_pw)) {			
						$this->frontendUserService->updateUserPassword($user->getUid(), $new_pw);
						$view['mode'] = 'password_changed';
						if ($pid = $this->settings['changePasswordRedirectPid']) {
							\nn\t3::Http()->redirect( $pid );
						}
					} else {
						$view['errors']['type'] = $user;
						$view['errors']['pw'] = 1;
					}
				}
			}
		}
				
		$this->view->assignMultiple( $view );
		return $this->renderView();
	}
	
	
}