<?php
namespace Nng\Nnfelogin\Wizards;


class FlexformTemplateWizard {
	
	function user_insertPredefTemplates( $config, $a = null ) {
		
		$ts = \nn\t3::Settings()->getPlugin('nnfelogin');

		if (!$ts['predef']) {
			$config['items'] = array( array('Kein TS gefunden - Template-Vorlagen können per plugin.tx_nnfelogin.predef definiert werden', '') );
			return $config;
		}
		
		foreach ($ts['predef'] as $k=>$v) {
			$label = $v['label'];
			$config['items'] = array_merge( $config['items'], array( array($label, $k, '') ) );
		}
		
		return $config;
	}
	
}
