<?php
namespace Nng\Nnfelogin\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;

class FrontendUserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 * @inject
	 */
	protected $configurationManager;

	/**
	 * 	Initialisierung
	 * 	@return void
	 */
	public function initializeObject () {
		$this->setConstraints();
	}

	/**
	 * 	Constraints setzen
	 * 	@return void
	 */
	public function setConstraints () {
			
		$settings = \nn\t3::Settings()->getMergedSettings();
		$querySettings = $this->objectManager->get(Typo3QuerySettings::class);

		if ($storagePid = $settings['storagePid']) {
			$querySettings->setStoragePageIds(
				GeneralUtility::trimExplode(',', $storagePid)
			);
		} else {
			$querySettings->setRespectStoragePage( false );
		}
		$this->setDefaultQuerySettings($querySettings);
	}
	
	
	/**
	 * action findOneByEmail
	 * Findet einen Benutzer anhand seiner E-Mail
	 * 
	 *  @param string $email
	 *  @return void|array
	 */
	public function findOneByEmail ( $email = null ) {
		$this->setConstraints();

		$email = trim($email);
		if (!$email) return false;
		return parent::findOneByEmail( $email );
	}
	
	
	/**
	 * action findByCustomField
	 * Prüft, ob Benutzer unter freiem Kriterium existiert, z.B. einer eigenen Spalte für Kunden-Nummer
	 * 
	 *  @param string $username
	 *  @param string|array $fields
	 *  @return void|array
	 */
	public function findByCustomField ( $username = null, $fields = null, $ignoreEnableFields = false, $returnAsArray = false ) {
		$this->setConstraints();

		$username = trim($username);
		if (!$username) return false;
		
		$fields = \nn\t3::Arrays( $fields )->trimExplode();
		$tcaColumns = \nn\t3::Db()->getColumns( 'fe_users' );
		$existingCols = array_intersect( $fields, array_keys($tcaColumns));

		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_users');
		$queryBuilder->select('*')->from('fe_users');

		if ($ignoreEnableFields) {
			$queryBuilder->getRestrictions()->removeAll();
		}
		
		foreach ($existingCols as $colName) {
			$queryBuilder->orWhere( 
				$queryBuilder->expr()->eq( $colName, $queryBuilder->createNamedParameter($username)
			));
		}

		$user = $queryBuilder->execute()->fetchAll();
		return $user;
	}
	
}