<?php

namespace Nng\Nnfelogin\Domain\Model;
use \TYPO3\CMS\Core\Utility\GeneralUtility as t3lib_div;


class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser {

	/**
	 * blocked
	 *
	 * @var string
	 */
	protected $blocked = '';

 	/**
     * @return string $blocked
     */
    public function getBlocked() {
        return $this->blocked;
	}

    /**
     * @param string $blocked
     * @return void
     */
    public function setBlocked($blocked) {
        $this->blocked = $blocked;
    }

}