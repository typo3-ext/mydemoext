<?php
namespace Nng\Nnfelogin\Domain\Service;

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Core\Utility\ArrayUtility;

use \Nng\Nnfelogin\Utilities\SettingsUtility;

use phpseclib\Crypt\RC4;
use phpseclib\Crypt\AES;
use phpseclib\Crypt\Random;

class EncryptionService {
	

    /**
     * 	Einen zufälligen Key erzeugen
     *	@return string
     */
	static public function generateRandomString($length = 50, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
		$pieces = [];
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$pieces[]= $keyspace[random_int(0, $max)];
		}
		return implode('', $pieces);
	}

    /**
     * 	Einen String verschlüsseln
     *	@return array
     */
    static public function encrypt($key, $payload, $useBase64 = true) {
		//$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
		$iv = self::generateRandomString(openssl_cipher_iv_length('aes-256-cbc'), 'acefghijk123456789');
		$encrypted = openssl_encrypt($payload, 'aes-256-cbc', $key, 0, $iv);
		if ($useBase64) $encrypted = base64_encode($encrypted);
		return [
			'data' 	=> $encrypted,
			'iv'	=> base64_encode($iv)
		];
	}

    /**
     * 	Einen String entschlüsseln
     *	@return string
     */	
	static public function decrypt($key, $iv, $garble, $useBase64 = true) {
		$iv = base64_decode($iv);
		if ($useBase64) $garble = base64_decode($garble);
		return openssl_decrypt($garble, 'aes-256-cbc', $key, 0, $iv);
	}
	
	
    /**
     * 	Cookie setzen, der von JS für verschlüsselte Übertragung verwendet wird.
     *	@return string
     */	
	public function setJsEncryptionCookie() {
		$key = $this->generateRandomString(30);
		setcookie('nnfelogin', $key, time()+3600, '/');	
		return $key;
	}
	
	public function getJsEncryptionCookie() {
		return $_COOKIE['nnfelogin'];
	}

    /**
     * 	Variablen im Array [enc][...] entschlüsseln
     *	@return array
     */	
	public function decryptRequestArguments( $arguments = [] ) {
		if (!$arguments['enc']) return $arguments;
		foreach ($arguments['enc'] as $k=>$v) {
			$arguments[$k] = self::cryptoJsAesDecrypt($this->getJsEncryptionCookie(), $v);
			unset($arguments['enc']);
		}
		return $arguments;
	}
	
	/**
	 * Decrypt data from a CryptoJS json encoding string
	 *
	 * @param mixed $passphrase
	 * @param mixed $jsonString
	 * @return mixed
	 */
	public static function cryptoJsAesDecrypt($passphrase, $jsonString){
		$jsondata = json_decode($jsonString, true);
		try {
			$salt = hex2bin($jsondata["s"]);
			$iv  = hex2bin($jsondata["iv"]);
		} catch(Exception $e) { return null; }

		$ct = base64_decode($jsondata["ct"]);
		$concatedPassphrase = $passphrase.$salt;
		
		//return ['salt'=>$salt, 'iv'=>$iv, 'ct'=>$ct];
		$md5 = array();
		$md5[0] = md5($concatedPassphrase, true);
		$result = $md5[0];
		for ($i = 1; $i < 3; $i++) {
			$md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
			$result .= $md5[$i];
		}
		$key = substr($result, 0, 32);
		$data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
		return json_decode($data, true);
	}

	
	/**
     * 	Eine E-Mail unkenntlich machen
     *	@return string
     */	
	public static function maskEmail( $str ) {
		if (!$str) return '';
		$parts = explode('@', $str);
		$domain_parts = explode('.', $parts[1]);
		
		$name = $parts[0];
		$toplevel = array_pop($domain_parts);
		$domain = join('.', $domain_parts);

		$lenName = strlen($name);
		$lenDomain = strlen($domain);
		
		return substr($name, 0, min(3, $lenName)).str_repeat('*', max(0,$lenName-3)).'@'.substr($domain, 0, min($lenDomain,4)).str_repeat('*', max(0,$lenDomain-4)).'.'.$toplevel;
	}

}