<?php

namespace Nng\Nnfelogin\Domain\Service;

use TYPO3\CMS\Sv\AuthenticationService;
use Nng\Nnfelogin\Domain\Service\EncryptionService;

class FrontendUserService extends AuthenticationService {
			

	/*
	 *	FE-User: Passwort vergessen Hash erzeugen und in DB schreiben
	 *  angelehnt an typo3/sysext/felogin/Classes/Controller/FrontendLoginController ab Zeile 385
	 *
	 */
	public function generateForgotPasswordParams ( $uid ) {

		if (!($uid = (int) $uid)) return false;
		
		// 12 Stunden gültig
		$validEnd = time() + 3600 * 12;
		$validEndString = date($this->conf['dateFormat'], $validEnd);
		$hash = EncryptionService::generateRandomString(30);
		$randHash = $validEnd . '|' . $hash;
		$randHashDB = $validEnd . '|' . md5($hash);
		
		\nn\t3::Db()->update('fe_users', ['felogin_forgotHash' => $randHashDB], $uid);

		return array('linkParams' => array(
			'user'			=> $uid,
			'forgothash'	=> $randHash
		));

	}
	
	/*
	 *	FE-User: Passwort Hash überprüfen
	 *  angelehnt an typo3/sysext/felogin/Classes/Controller/FrontendLoginController ab Zeile 302
	 *
	 */
	 
	public function validateForgotPasswordHash ( $uid, $hash ) {
		if (!($uid = (int) $uid)) return false;
		$hash = explode('|', $hash);
		
		$user = &$GLOBALS['TSFE']->fe_user->getRawUserByUid($uid);
		if (!$user) false;
		
		$userHash = $user['felogin_forgotHash'];
		
		$compareHash = explode('|', $userHash);
		if (!$compareHash || !$compareHash[1] || $compareHash[0] < time() || $hash[0] != $compareHash[0] || md5($hash[1]) != $compareHash[1]) return false;
		return true;
	}
	
	
	/*
	 *	FE-User: Passwort ändern
	 *	@return string
	 */
	public function updateUserPassword ( $uid, $newPassword ) {

		$uid = intval($uid);
		if (!$uid) return false;
		
		$saltedPassword = \nn\t3::Encrypt()->password( $newPassword );

		\nn\t3::Db()->update( 'fe_users', [
			'password' => $saltedPassword,
			'pwchanged' => time(),
		], $uid);

		return $saltedPassword;
	}
	

}
	