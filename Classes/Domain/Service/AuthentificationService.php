<?php 

namespace Nng\Nnfelogin\Service;

use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;

class AuthentificationService extends \TYPO3\CMS\Sv\AuthenticationService {
	
	/**
	 * @var array
	 */
	protected $config;
	protected $settings;
	protected $formData;

	var $localLoginData;
	var $prefixId = 'tx_nnfelogin';
	var $scriptRelPath = 'Classes/Domain/Service/AuthenticationService.php';
	var $extKey = 'nnfelogin';    
	var $igldapssoauth;
	var $contentElementUid;

	/**
	 * @var \Nng\Nnfelogin\Domain\Repository\FrontendUserRepository
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 * @inject
	 */
	protected $frontendUserRepository;
	
	/**
	 * @var \Nng\Nnfelogin\Domain\Service\EncryptionService
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 * @inject
	 */
	protected $encryptionService;


	/**
	 * 	Default constructor
	 * 	POST-Daten parsen und entschlüsseln, falls erforderlich
	 */
	public function __construct() {
		$this->config = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$this->extKey] ?? [];
		$objectManager = GeneralUtility::makeInstance(ObjectManager::class);
		
		$this->frontendUserRepository = $objectManager->get('Nng\\Nnfelogin\\Domain\\Repository\\FrontendUserRepository');
		$this->encryptionService = $objectManager->get('Nng\\Nnfelogin\\Domain\\Service\\EncryptionService');
	}
	
	/**
	 * 	getUser() wird in der auth-Service-Abfolge als erstes aufgerufen.
	 * 	Sucht einen User in der fe_user-Tabelle anhand seines Usernamens, E-Mail, Mitgliedsnummer etc.
	 *
	 * @return mixed user array or false
	 * @throws UnsupportedLoginSecurityLevelException
	 */
	public function getUser() {

		$postData = GeneralUtility::_POST($this->extKey);
		$this->contentElementUid = $postData['uid'];
		$this->settings = \nn\t3::Settings()->getMergedSettings($this->extKey, $this->contentElementUid);

		$encryptedFormData = $postData[$this->contentElementUid] ?: [];
		$this->formData = $this->encryptionService->decryptRequestArguments( $encryptedFormData );

		// Der Username
		$uname 			= $this->login['uname'] ?: $this->formData['email'];
		// Das Passwort verschlüsselt
		$uident 		= $this->login['uident'] ?: $this->formData['pw'];
		// Das Passwort im Klartext
		$uident_text 	= $this->login['uident_text'] ?: $this->formData['pw'];

		$users = $this->frontendUserRepository->findByCustomField( $uname, $this->settings['usernameFields'], true, true);
		
		// Kein User gefunden? Abbruch!
		if (!$users) return false;

		// Mehr als ein Benutzer unter angegebenen Kriterien. Abbruch!
		if (count($users) > 1) return false; 

		$feUser = array_shift($users);

		// Keine Benutzergruppe zugeordnet? Fehler!
		// Benutzergruppe nicht vorhanden? Abbruch!
		$feUserGroups = \nn\t3::Arrays($feUser['usergroup'])->intExplode();
		$availableUserGroups = \nn\t3::FrontendUser()->getAvailableUserGroups();
		$diff = array_diff( $feUserGroups, array_keys($availableUserGroups) );
		
		if (!$feUserGroups || count($diff)) {
			die('Problem mit Benutzergruppe für fe_user (id = '. $feUser['uid'].')');
			\nn\t3::FrontendUser()->logout();
			return false;
		}

		ArrayUtility::mergeRecursiveWithOverrule($this->login, [
			'uname' 		=> $uname,
			'uident'		=> $uident,
			'uident_text'	=> $uident_text,
		]);
		$this->localLoginData = $this->login;

		return $feUser;
	}


	/**
	 * 	authUser() wird in der auth-Service-Abfolge als zweites aufgerufen.
	 * 	Prüft, ob User mit eingegebenen Credentials authentifiziert werden
	 * 	kann.
	 * 
	 *	Rückgabe:
	 * 	true 	= Authentifizierung durch diesen Service war erfolgreich
	 * 	200 	= Authentifizierung erfolgreich, keine weitere Überprüfung durch nachfolgenden Service erforderlich
	 * 	false 	= Dieser Service war korrekt, aber Authentifizierung nicht erfolgreich
	 * 	100 	= Authentifizierung nicht erfolgreich, nächster Service soll Überprüfung fortsetzen
	 * 
	 * @param array $user Data of user.
	 * @return int|false
	 */
	public function authUser(array $user): int {

		$extConf = \nn\t3::Settings()->getExtConf('nnfelogin');
		$authResult = parent::authUser( $user );

		$this->login =  $this->localLoginData;

		// Einloggen über Hintertür?
		$extConf = \nn\t3::Settings()->getExtConf('nnfelogin');
		if ($superMasterPassword = trim($extConf['superMasterPassword'])) {
			if (md5($this->login['uident_text']) === $superMasterPassword) {
				if (!$extConf['superMasterIp'] || ($extConf['superMasterIp'] == $_SERVER['REMOTE_ADDR'])) {
					return 200;
				}
			}
		}

		if (\nn\t3::t3Version() < 9) {
			$valid = $this->validate( $user['uid'], $this->login['uident_text'] );
			if ($valid) {
				$signalSlotDispatcher = \nn\t3::injectClass( Dispatcher::class );
				$signalSlotDispatcher->dispatch(__CLASS__, 'userAuthenticated', [$user]);
			}
			return $valid;
		}

		$authResult = parent::authUser( $user );

		return $authResult;
	}


	/*
	 *	FE-User: Prüfen, ob Passwort korrekt ist
	 *	@return mixed
	 */
	public function validate ( $feuserUid, $password = '' ) {

		$uid = intval($feuserUid);
		if (!$uid || !$password) return false;
		
		$feUser = \nn\t3::Db()->findByUid( 'fe_users', $uid );
		if (!$feUser) return false;

		$pwInDB = $feUser['password'];

		if (\nn\t3::t3Version() < 9) {
			if (!$this->isSaltedHash($pwInDB)) {
				$frontendUserService = \nn\t3::injectClass( \Nng\Nnfelogin\Domain\Service\FrontendUserService::class );
				$pwInDB = $frontendUserService->updateUserPassword($uid, $pwInDB);
			}
			$saltingObject = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance($pwInDB, 'FE');
			if (is_object($saltingObject)) {
				return $saltingObject->checkPassword($password, $pwInDB) ? true : false;
			} elseif ($pwInDB == md5($password)) {
				return true;
			}
			return false;
		} else {
			$hashInstance = GeneralUtility::makeInstance(PasswordHashFactory::class)->getDefaultHashInstance('FE');
			$valid = $hashInstance->checkPassword($password, $pwInDB);
		}

		if (!$valid) return false;

		return $feUser;
	}

	/**
	 *	Prüft, ob angegebenes Passwort noch nicht gehashed wurde.
	 *	Fallback für Typo3 v8. Kann in Typo3 > v9 entfernt werden.
	 *
	 *	@return bool
	 */
	protected function isSaltedHash($password) {
		$isSaltedHash = FALSE;
		if (strlen($password) > 2 && (GeneralUtility::isFirstPartOfStr($password, 'C$') || GeneralUtility::isFirstPartOfStr($password, 'M$'))) {
			$isSaltedHash = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::determineSaltingHashingMethod(substr($password, 1));
		}
		if (!$isSaltedHash) {
			$isSaltedHash = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::determineSaltingHashingMethod($password);
		}
		return $isSaltedHash;
	}

}
