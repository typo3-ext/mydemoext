<?php

defined('TYPO3_MODE') || die('Access denied.');


$temporaryColumns = [
	'pwchanged' => [
		'exclude' => 1,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'Letzte Passwort-Änderung',
		'config' => [
			'type' => 'input',
			'size' => 16,
			'max' => 20,
			'eval' => 'datetime',
			'default' => 0,
		]
	],
	'blocked' => [
		'exclude' => 1,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'Benutzer sperren?',
		'config' => [
			'type' => 'check',
			'items' => [
				['gesperrt', ''],
			],
		],
	],
	'member_uid' => [
		'exclude' => 1,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'Mitglieds-Nummer',
		'config' => [
			'type' => 'input',
			'size' => 16,
			'default' => '',
		]
	],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
	'fe_users',
	$temporaryColumns
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'fe_users',
	'pwchanged',
	'blocked',
	'member_uid',
	'',
	'after:password'
);