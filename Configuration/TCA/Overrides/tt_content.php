<?php
defined('TYPO3_MODE') or die();

/***************
 * Plugin
 */

 \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'nnfelogin',
    'Pi1',
    'NN Frontend Login'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['nnfelogin_pi1'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['nnfelogin_pi1'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('nnfelogin_pi1',
    'FILE:EXT:nnfelogin/Configuration/FlexForms/flexform.xml');
