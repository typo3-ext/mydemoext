
## ----------------------------------------------------------------------------
## settings
## ----------------------------------------------------------------------------

plugin.tx_nnfelogin {
	
	settings {
		storagePid = 
		
		forgotPasswordPid = 117
		
		forgotPassword {
			subject = Passwort zurücksetzen
			fromEmail = info@99grad.de
			fromName = Webmaster
			returnPid = 164
		}
		
		# Diese Felder werden als alternative User-Name verwendet
		# usernameFields = username, email, member_uid	
		usernameFields = username, email, personalnummer	

		# Content-Elemente für Textbausteine
		ce {
			# Einleitungstext zu "Sie haben ihr Passwort vergessen?"
			resetPassword =
			# Text in der Mail
			mailtext =
			# Infotext zu "Mail wurde versendet"
			mailsent =
			# Infotext zu "Bitte neues Passwort eingeben"
			enterNewPassword = 
			# Infotext zu "Passwort wurde geändert" 
			pwChanged =
		}	
	}

	view {
		templateRootPaths {
			0 = EXT:nnfelogin/Resources/Private/Templates/Default/
		}
		partialRootPaths {
			0 = EXT:nnfelogin/Resources/Private/Partials/
		}
		layoutRootPaths {
			0 = EXT:nnfelogin/Resources/Private/Layouts/
		}
	}

	# Templates (Views) können hier definiert werden und sind dann als Dropdown im Flexform verfügbar
	predef {
		
		default {
			label = Standard
			templateRootPaths {
				10 = EXT:nnfelogin/Resources/Private/Templates/Default/
			}
			partialRootPaths {
				10 = EXT:nnfelogin/Resources/Private/Templates/Default/Partials/
			}		
		}
			
		status < .default
		status {
			label = Status-Zeile
			templateRootPaths {
				10 = EXT:nnfelogin/Resources/Private/Templates/Status/
			}
		}
		
	}
	
	prefdefStyles {
		
	}
	
	persistence {
		storagePid =
	}
	
	mvc {
		callDefaultActionIfActionCantBeResolved = 1
	}
	
	features {
		skipDefaultArguments = 1
	}
	
	_CSS_DEFAULT_STYLE >
	
}


config.tx_extbase{
    persistence{
        classes{

            TYPO3\CMS\Extbase\Domain\Model\FrontendUser {
                subclasses {
                    Tx_Nnfelogin_FrontendUser = Nng\Nnfelogin\Domain\Model\FrontendUser
          		}
            }
          
            Nng\Nnfelogin\Domain\Model\FrontendUser {
                mapping {
                    tableName = fe_users
                }
            }
            
    	}
    }
}

## ----------------------------------------------------------------------------
## ... und ein Plugin für alle Seiten bereitstellen
## ----------------------------------------------------------------------------

/*
lib.nnfelogin_status = USER
lib.nnfelogin_status {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    extensionName = Nnfelogin
   	pluginName = Pi1
    vendorName = Nng
    controller = Main
    action = showLoginForm
    switchableControllerActions {
        Main {
        	1 = showLoginForm
        }
    }
    view < plugin.tx_nnfelogin.view
    persistence < plugin.tx_nnfelogin.persistence
    settings < plugin.tx_nnfelogin.settings
    
    settings.flexform {
    	predefTemplate = status
    	loginFormPid = 8
    }
}
*/

##page.1000 < lib.nnfelogin_status





