<?php

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function( $extKey )
    {
				
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
			'Nng.' . $extKey,
			'Pi1',
			[
				'Main' => 'showLoginForm, showForgotPasswordForm, showChangePasswordForm, form',
				
			],
			// non-cacheable actions
			[
				'Main' => 'showLoginForm, showForgotPasswordForm, showChangePasswordForm, form',
				
			]
		);

		// Icon registrieren
		\nn\t3::Registry()->icon('nnfelogin-plugin-main', 'EXT:nnfelogin/Resources/Public/Icons/wizicon.svg');

		// Page-Config einbinden	
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
			<INCLUDE_TYPOSCRIPT: source="FILE:EXT:nnfelogin/Configuration/TypoScript/page.txt">
		');

		// -----------------------------------------------------------------------------------
		// eID Dispatcher

		$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['nnfelogin'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('nnfelogin').'Classes/Dispatcher/EidDispatcher.php';


		// Slot Registrierungen
		$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\SignalSlot\Dispatcher');
		$signalSlotDispatcher->connect(
			'TYPO3\EsweApi\Domain\Service\UserService', 
			'emitResetPassword', 
			'Nng\Nnfelogin\Controller\MainController', 
			'resetPasswordDispatcher'
		);

		// -----------------------------------------------------------------------------------
		// AUTH-Service Registrierung

		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addService(
			'nnfelogin_auth Auth',
			'auth',
			\Nng\Nnfelogin\Service\AuthentificationService::class,
			[
				'title' => 'Authentification service (nnfelogin)',
				'description' => 'Authentication service für Login über E-Mail und Mitgliedsnummer.',
				'subtype' => 'getUserFE,authUserFE,getGroupsFE',
				'available' => true,
				'priority' => 81,
				'quality' => 81,
				'os' => '',
				'exec' => '',
				'className' => \Nng\Nnfelogin\Service\AuthentificationService::class,
			]
		);

		// -----------------------------------------------------------------------------------
		// composer Autoload

		require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath( $extKey ) . '/Resources/Libraries/vendor/autoload.php';
		
	}, 
$_EXTKEY );


