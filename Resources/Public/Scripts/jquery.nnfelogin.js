

$(function () {
	$('.nnfelogin').each(function () {
		$(this).nnfelogin();
	});
});

(function ($){

	$.fn.extend({
		'nnfelogin': function ( opt ) {
				
			return this.each(function () {
				var $me = $(this);
				var $form = $me.find('form');

				$form.find('input').focus(function () {
					$(this).removeClass('has-error');
				});

				$form.submit( function (e) {
					
					$form.find('.enc').remove();
					$form.addClass('loading');
					
					$me.find('[data-encrypt]').each(function () {
						var $el = $(this);
						var name = $el.prop('name');
						var newName = name.split('][').join('][enc][');
						var $newEl = $('<input type="hidden" name="'+newName+'" value="" class="enc" />');
						$newEl.val( $el.val() ).nnfelogin_enigmaEncrypt();
						$form.append($newEl);
						$el.val('').blur();
					});

					if ($form.find('[data-ajax]').length) {
						$form.nnfelogin_ajaxPostForm({complete:function ( data ) {

							var data = $.parseJSON(data);
							if (data.errors && (data.errors.pw == 1 || data.errors.noGroup == 1)) {
								$form.find('input[type="text"], input[type="password"]').addClass('has-error');
								$form.addClass('show-error-msg');
								$form.find('input[type="text"]').focus();
							}
							if (data.redirect) {
								window.location.href = data.redirect;
							} else {
								$form.removeClass('loading');
							}
						}});
						return false;
					}
				});
				
			});
		}
	});


	$.extend({
		'nnfelogin_enigmaEncrypt': function ( decryptedContent, key ) {
			return CryptoJS.AES.encrypt(JSON.stringify(decryptedContent), key, {format: $.nnfelogin_CryptoJSAesJson}).toString();
		},
		'nnfelogin_getCookie': function (cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');

			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		},
		'nnfelogin_getAesKey': function () {
			return [$.nnfelogin_getCookie('nnfelogin')].join('');
		},
		'nnfelogin_CryptoJSAesJson': {
			stringify: function (cipherParams) {
				var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
				if (cipherParams.iv) j.iv = cipherParams.iv.toString();
				if (cipherParams.salt) j.s = cipherParams.salt.toString();
				return JSON.stringify(j);
			},
			parse: function (jsonStr) {
				var j = JSON.parse(jsonStr);
				var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
				if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
				if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
				return cipherParams;
			}
		}
	});

	$.fn.extend({
		'nnfelogin_enigmaEncrypt': function ( key ) {
			if (!key) key = $.nnfelogin_getAesKey();
			var $me = $(this);
			var decryptedContent = $me.val();
			var encryptedContent = $.nnfelogin_enigmaEncrypt(decryptedContent, key);
			$me.val(encryptedContent);
		},

		'nnfelogin_ajaxPostForm': function ( params ) {
		
			var $form = $(this);
			params = $.extend({
				url: $form.attr('action'),
				postData: $form.nnfelogin_serialzeFormData(),
				complete: false,
				type: 'POST',
				fail: false
			}, params);
			
			$.ajax({
				type: params.type,
				url: params.url,
				data: params.postData
			}).done(function ( data ) {
				if (params.complete) params.complete.apply( $form, [data] );
			}).fail(function (data) {
				if (params.fail) params.fail.apply( $form, [data] );				
			});
		},
		
		'nnfelogin_serialzeFormData': function () {
			var data = {};
			$(this).find('input, select, textarea, button').each( function () {
				var $field = $(this);
				var key = $field.attr('name');
				var v = $field.val();
				
				if ($field.attr('type') == 'radio' || $field.attr('type') == 'checkbox') {
					var $selected = $('[name="'+$field.attr('name')+'"]').filter(':checked');
					if ($selected.length) {
						v = $selected.val();
					} else {
						v = '';
					}
				}
				
				if (key) {
					data[key] = v;
				}
			});
			return data;
		}

	});

})(jQuery);
